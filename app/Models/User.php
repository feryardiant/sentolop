<?php declare (strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * @property string $display
 * @property string $username
 * @property string $email
 * @property string $active_at
 * @property string $banned_at
 * @property string $ban_reasons
 * @property string $api_token
 * @property-read bool $is_active
 * @property-read bool $is_banned
 * @property-read User|Builder $banned
 * @property-read User|Builder $active
 * @method static User|Builder banned(bool $banned = true)
 * @method static User|Builder active(bool $active = true)
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;
    use SoftDeletes;

    protected $fillable = [
        'display', 'username', 'email', 'password',
        'active_at', 'banned_at', 'ban_reasons',
        'api_token'
    ];

    protected $hidden = ['password', 'remember_token', 'api_token'];

    protected $dates = ['active_at', 'banned_at'];

    protected static function boot()
    {
        parent::boot();

        static::saving(function (User $model) {
            if (! $model->getAttributeValue('username')) {
                $model->setAttribute('username', $model->getAttributeValue('email'));
            }
        });
    }

    /**
     * Automatically hash password value.
     *
     * @param  string  $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = $value ? \Hash::make($value) : null;
    }

    /**
     * Mutator attribute that determine this model is activated or not.
     *
     * @return bool
     * @SuppressWarnings(PHPMD)
     */
    public function getIsActiveAttribute()
    {
        return null !== $this->getAttributeValue('active_at');
    }

    /**
     * Mutator attribute that determine this model is banned or not.
     *
     * @return bool
     * @SuppressWarnings(PHPMD)
     */
    public function getIsBannedAttribute()
    {
        return null !== $this->getAttributeValue('banned_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|Role[]
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Setting[]
     */
    public function settings()
    {
        return $this->hasMany(Setting::class);
    }

    /**
     * @param  bool  $bool
     * @param  string|null  $reasons
     * @return bool|null
     */
    public function ban(bool $bool = true, $reasons = null)
    {
        if ($this->getIsBannedAttribute() === $bool) {
            return null;
        }

        $value = [
            'banned_at' => (bool) $bool ? $this->freshTimestamp() : null,
            'ban_reasons' => (bool) $bool ? $reasons : null
        ];

        if ($this->update($value)) {
            $this->fireModelEvent((bool) $bool ? 'banned' : 'disbanned');

            return true;
        }

        return false;
    }

    /**
     * @param  \Illuminate\Database\Query\Builder  $query
     * @param  bool  $query
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeBanned($query, bool $bool = true)
    {
        return (bool) $bool
            ? $query->whereNotNull('banned_at')
            : $query->whereNull('banned_at');
    }

    /**
     * @see Activable::activate()
     *
     * @param  bool  $bool
     * @return bool|null
     */
    public function activate(bool $bool = true)
    {
        // Don't do anything when the current model is already has same value as $bool param.
        if ($this->getIsActiveAttribute() === $bool) {
            return null;
        }

        $value = [
            'active_at' => (bool) $bool ? $this->freshTimestamp() : null
        ];

        if ($this->update($value)) {
            $this->fireModelEvent((bool) $bool ? 'activated' : 'inactivated');

            return true;
        }

        return false;
    }

    /**
     * @param  \Illuminate\Database\Query\Builder  $query
     * @param  bool  $query
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeActive($query, bool $bool = true)
    {
        return (bool) $bool
            ? $query->whereNotNull('active_at')
            : $query->whereNull('active_at');
    }
}
