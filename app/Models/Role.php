<?php declare (strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use\Illuminate\Database\Eloquent\Builder;

/**
 * @property string $name
 * @property string $label
 * @property string $description
 * @property bool $is_default
 * @property object $scopes
 * @property-read Role|Builder $default
 * @method static Role|Builder default()
 */
class Role extends Model
{
    const ADMIN = 'admin';
    const USER = 'user';

    protected $filterKey = 'name';

    protected $fillable = [
        'name', 'slug', 'description', 'is_default', 'scopes'
    ];

    protected $casts = [
        'is_default' => 'boolean',
        'scopes' => 'json',
    ];

    public function getNameAttribute($value)
    {
        return $value ?: trans("roles.{$this->slug}.name");
    }

    public function getDescriptionAttribute($value)
    {
        return $value ?: trans("roles.{$this->slug}.description");
    }

    /**
     * Show 'default' roles only.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeDefault(Builder $query)
    {
        return $query->where('is_default', true);
    }

    /**
     * Set the model as default role for new users.
     *
     * @param  Builder  $query
     * @return bool
     */
    public function makeDefault()
    {
        if ($this->is_default) {
            return false;
        }

        if ($previous = Role::default()->first()) {
            $previous->update([
                'is_default' => false
            ]);
        }

        return $this->update([
            'is_default' => true
        ]);
    }

    /**
     * Retrieve relation object.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|User
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
