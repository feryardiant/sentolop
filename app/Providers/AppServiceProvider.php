<?php declare (strict_types = 1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootSiteMeta();

        if (env('FORCE_HTTPS', false)) {
            $this->app->get('url')->forceScheme('https');
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    protected function bootSiteMeta()
    {
        $appConfig = $this->app->get('config')->get('app');
        $meta = [
            'short_name' => $appConfig['short_name'],
            'description' => $appConfig['description'],
            'theme_color' => $appConfig['theme_color'],
            'icons' => []
        ];

        foreach ($appConfig['icons'] as $size => $path) {
            $meta['icons'][] = [
                'src' => asset($path),
                'type' => 'image/' . pathinfo($path, PATHINFO_EXTENSION),
                'sizes' => $size
            ];
        }

        $meta['icon'] = $meta['icons'][count($meta['icons']) - 1];

        \View::share([
            'meta' => collect($meta)
        ]);
    }
}
