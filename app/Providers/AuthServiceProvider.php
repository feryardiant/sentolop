<?php declare (strict_types = 1);

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use App\Models\Role;
use Illuminate\Support\Arr;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        foreach ($this->getAvailableScopes() as $scope) {
            Gate::define($scope, function (User $user) use ($scope) {
                if (! $user->roles) {
                    return false;
                }

                if ($user->roles->contains('name', Role::ADMIN)) {
                    return true;
                }

                return $user->roles->pluck('scopes')
                    ->flatten()
                    ->intersect($scope)
                    ->isNotEmpty();
            });
        }
    }

    public function register()
    {
        // \Auth::provider('app.user', function ($app, $config) {
        //     return new Auth\UserProvider($app['hash'], $config['model']);
        // });
    }

    /**
     * Let's simply list all available routes as permission scopes.
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getAvailableScopes()
    {
        $guard = $this->app->get('config')->get('auth.defaults.guard');
        $permissions = [];

        foreach (\Route::getRoutes() as $route) {
            if (in_array($guard, (array) $route->middleware())) {
                $permission = $route->getName();

                Arr::set($permissions, $permission, $permission);
            }
        }

        return collect($permissions)->flatten();
    }
}
