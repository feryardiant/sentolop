<?php declare (strict_types = 1);

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function show(Request $request)
    {
        return [];
    }

    public function update(Request $request)
    {
        return [];
    }
}
