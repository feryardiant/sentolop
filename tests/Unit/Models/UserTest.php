<?php

namespace Tests\Unit\Models;

// use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    protected function createNewUser()
    {
        return User::firstOrCreate(
            ['email' => 'john@doe.com'],
            ['password' => 'secret']
        );
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHashPasswordFieldOnCreatingUser()
    {
        $user = $this->createNewUser();

        $this->assertTrue(password_verify('secret', $user->password));
    }

    public function testCreateUsernameFromEmail()
    {
        $user = $this->createNewUser();

        $this->assertEquals($user->email, $user->username);
    }

    public function testActivateUser()
    {
        $user = $this->createNewUser();

        $this->assertFalse($user->is_active);

        $user->activate();

        $this->assertTrue($user->is_active);
    }

    public function testGetActiveUsers()
    {
        factory(User::class, 3)->create([
            'active_at' => null
        ]);

        $this->assertCount(0, User::active(true)->get());
        $this->assertCount(3, User::active(false)->get());
    }

    public function testBan()
    {
        $user = $this->createNewUser();

        $this->assertFalse($user->is_banned);

        $user->ban(true, $reasons = 'You are ugly');

        $this->assertTrue($user->is_banned);
        $this->assertEquals($reasons, $user->ban_reasons);

        $user->ban(false);

        $this->assertFalse($user->is_banned);
        $this->assertNull($user->ban_reasons);
    }

    public function testGetBannedUsers()
    {
        factory(User::class, 3)->create();

        $this->assertCount(0, User::banned(true)->get());
        $this->assertCount(3, User::banned(false)->get());
    }
}
