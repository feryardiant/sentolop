function resourceRoutes (resources, parent = '') {
  const routes = []
  const isChild = parent.length > 0

  for (const [name, options] of Object.entries(resources)) {
    const { path, children, ...meta } = Object.assign({}, {
      path: name,
      auth: true,
      title: '',
      icon: '',
      children: {},
      transition: 'fade'
    }, options)

    const route = {
      path: path || name,
      meta: {
        hasChildren: Object.keys(children).length > 0,
        ...meta
      },
      component: () => {
        const component = !!path && path.includes('/') ? 'index' : 'child'

        return import(/* webpackChunkName: "resources" */ `@/views/resource/${component}.vue`)
      }
    }

    route.children = [
      {
        path: '',
        name: [name, 'index'].join('.'),
        component: () => import(/* webpackChunkName: "resource-index" */ '@/views/resource/table.vue'),
        meta: { ...meta, menu: 'All' }
      },
      {
        path: 'create',
        name: [name, 'create'].join('.'),
        component: () => import(/* webpackChunkName: "resource-create" */ '@/views/resource/form.vue'),
        meta: { ...meta, menu: false }
      },
      ...(route.meta.hasChildren ? resourceRoutes(children, name) : []),
      {
        path: ':id',
        name: [name, 'edit'].join('.'),
        component: () => import(/* webpackChunkName: "resource-edit" */ '@/views/resource/form.vue'),
        meta: { ...meta, menu: false }
      }
    ].map(child => {
      if (!Array.isArray(child.children)) {
        child.name = (isChild ? [parent, child.name] : [child.name]).join('.')
      }

      return child
    })

    routes.push(route)
  }

  return routes
}

export default resourceRoutes
