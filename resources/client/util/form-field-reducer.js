import { timesAgo } from './dateFormater'

export function field ({ key, label, desc, type, attrs, ...item }) {
  const obj = {
    key,
    label,
    desc,
    type,
    attrs: Object.assign({}, item, attrs)
  }

  if (['panel', 'group'].includes(type)) {
    delete obj.attrs.fields
    obj.fields = item.fields.map(field)
  }

  return obj
}

function cellWidth (item) {
  if (['id', 'actions'].includes(item.key)) {
    return '3%'
  }

  switch (item.type) {
    case 'number':
    case 'currency':
    case 'integer':
    case 'image':
      return '10%'
    case 'date':
    case 'datetime':
    case 'timestamp':
      return '14%'
    default:
      return 'auto'
  }
}

/**
 * Column reducer.
 *
 * @param  {Array}  [fields=[]]  Field definitions.
 * @return {Array}
 */
export function column (fields = []) {
  if (fields.length === 0) {
    const first = this.data ? Object.assign({}, this.data[0] || {}) : {}

    for (const field of Object.keys(first)) {
      fields.push({
        key: field,
        visible: true
      })
    }
  }

  fields.push({
    key: 'actions',
    sortable: false,
    visible: true,
    order: fields.length + 2
  })

  // const typesRight = ['date', 'datetime', 'timestamp']
  // const typesCenter = []

  return fields.reduce((cols, col) => {
    if (['panel', 'group'].includes(col.type)) {
      col.fields.forEach(child => {
        cols.push(child)
      })
    } else {
      cols.push(col)
    }

    return cols
  }, []).filter(col => col.visible).map((col) => {
    let key = col.key.replace('_', '-')

    col.isHtml = false
    col.order = col.order || (col.key === 'id' ? -1 : 1)
    col.class = [`column-key-${key}`, `column-type-${(col.type || 'default')}`]
    col.thStyle = Object.assign({}, col.columnStyle || {}, {
      width: cellWidth(col)
    })

    if (col.sortable && col.key !== this.primary) {
      col.class.push('column-sortable')
    }

    if (['date', 'datetime', 'timestamp'].includes(col.type)) {
      // col.class.push('text-center')
      col.isHtml = true
      col.formatter = (value) => {
        return timesAgo(value, { addSuffix: true })
      }
    } else if (['number', 'currency', 'int', 'integer'].includes(col.type)) {
      col.class.push('text-right')
      col.formatter = (value) => {
        return Number(value).toLocaleString(process.env.VUE_APP_LOCALE)
      }
    }

    return col
  }).sort((a, b) => {
    return a.order - b.order
  })
}

/**
 * Form Panel reducer.
 *
 * @param  {Array}  [fields=[]]  Field definitions.
 * @return {Array}
 */
export function panels (fields = []) {
  return fields.reduce((panels, field) => {
    if (field.type === 'panel') {
      panels.push(field)
      return panels
    }

    const panel = panels.find(p => p.key === 'main')

    if (panel) {
      panel.fields.push(field)
    } else {
      panels.push({
        key: 'main',
        type: 'panel',
        attrs: {
          visible: true
        },
        fields: [field]
      })
    }

    return panels
  }, []).map(({ fields, attrs, ...panel }) => {
    // Hide panel when no fields are visible
    attrs.visible = fields.reduce((sum, { attrs }) => sum + Number(attrs.visible), 0) > 0

    return { fields, attrs, ...panel }
  }).filter(field => field.attrs.visible)
}
