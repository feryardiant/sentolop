import Vue from 'vue'
import Vuex from 'vuex'
import { basename } from 'path'

Vue.use(Vuex)

const modules = {}
const loadModules = require.context('.', false, /\.js$/)

loadModules.keys().forEach(module => {
  if (module === './index.js') return

  const key = basename(module, '.js')

  modules[key] = {
    namespaced: true,
    ...loadModules(module)
  }
})

export default new Vuex.Store({
  modules,

  state: {
    $busy: false
  },

  mutations: {
    busy (state) {
      state.$busy = true
    },

    done (state) {
      state.$busy = false
    }
  }
})
