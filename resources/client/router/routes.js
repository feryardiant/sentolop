import Home from '@/pages/home'
import NotFound from '@/pages/error-404'

export default [
  {
    path: '/',
    name: Home.name,
    component: Home,
    meta: {
      title: 'Dashboard',
      icon: 'home'
    }
  },

  {
    path: '*',
    name: NotFound.name,
    component: NotFound,
    meta: {
      title: 'Page not found',
      menu: false
    }
  }
]
