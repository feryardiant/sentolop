const layouts = {}
const loadLayouts = require.context('.', false, /\.vue$/)

loadLayouts.keys().forEach(key => {
  const { default: layout } = loadLayouts(key)
  layouts[layout.name] = layout
})

export default layouts
