import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import nProgress from 'nprogress'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.less'

import './bootstrap'
import { getMeta } from './util'
import layouts from './layouts'
import router from './router'
import store from './store'

Vue.config.productionTip = false

nProgress.configure({
  showSpinner: false
})

const appName = getMeta('generator', 'Sentolop')

router.beforeEach((to, from, next) => {
  document.body.className = document.body.className.replace(from.name, to.name) || to.name
  document.title = `${appName} - ${to.meta.title}`

  next()
})

sync(store, router)

Vue.use(Antd)

const $app = window.$app = new Vue({
  name: 'app',

  template: `<component :is="layout"></component>`,

  components: {
    ...layouts
  },

  computed: {
    key () {
      const normalizedPath = this.$route.path.slice(1).split('/').join('-')

      return normalizedPath || this.$route.name
    },

    layout () {
      const layout = this.$route.meta.layout || 'base'

      return `${layout}-layout`
    }
  },

  router,

  store
})

$app.$mount('#app')
