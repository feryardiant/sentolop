import { mapState, mapMutations } from 'vuex'

export const computed = mapState(['$busy'])

export const methods = mapMutations(['busy', 'done'])
