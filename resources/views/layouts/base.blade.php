<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }}</title>

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="theme-color" content="#4285f4">
    <meta name="robots" content="index,follow">
    <meta name="api-url" content="{{ url('api') }}">
    <meta name="generator" content="{{ config('app.name') }}">

    <link rel="apple-touch-icon" sizes="192x192" href="{{ asset('favicons/mobile-icon.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('favicons/mobile-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon.png') }}">

    {{-- Page stylesheets --}}
    @yield('site-head')
</head>
<body>
    <noscript>
        <strong>We're sorry but this app doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>

    @yield('site-body')

    {{-- Page scripts--}}
    @yield('site-foot')
</body>
</html>
