@extends('layouts.base', ['class' => ['landing-page']])

@section('site-head')
    <link rel="stylesheet" href="{{ mix('css/main.css') }}">
@endsection

@section('site-body')
    <div id="app"></div>
@endsection

@section('site-foot')
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/main.js') }}"></script>
@endsection
