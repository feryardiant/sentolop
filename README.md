# Sentolop

[![pipeline status](https://gitlab.com/creasico/core/sentolop/badges/master/pipeline.svg)](https://gitlab.com/creasico/core/sentolop/commits/master)
[![coverage report](https://gitlab.com/creasico/core/sentolop/badges/master/coverage.svg)](https://gitlab.com/creasico/core/sentolop/commits/master)


## Sponsors

- [BrowserStack](https://www.browserstack.com/automate)

## License

This project is open-sourced software licensed under the [MIT license](LICENSE.md).
