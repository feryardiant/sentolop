<?php
/**
 * Copyright (c) 2018.  Fery Wardiyanto <ferywardiyanto@gmail.com>
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug', 100)->unique();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->json('scopes')->default('[]');
            $table->boolean('is_default')->default(false);
            $table->timestamps();
        });

        Schema::create('role_user', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')
                ->references('id')->on('users')
                ->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->unsignedInteger('role_id')
                ->references('id')->on('roles')
                ->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_user');
        Schema::dropIfExists('roles');
    }
}
