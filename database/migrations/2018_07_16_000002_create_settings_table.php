<?php
/**
 * Copyright (c) 2018.  Fery Wardiyanto <ferywardiyanto@gmail.com>
 */

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()
                ->references('id')->on('users')
                ->onDelete('CASCADE')->onUpdate('CASCADE');

            $table->string('slug', 100)->unique();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('type');
            $table->longText('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
