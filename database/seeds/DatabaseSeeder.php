<?php

use App\Models;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected function users()
    {
        return [
            Models\Role::ADMIN => [
                'display' => 'Administrator',
                'email' => 'admin@example.com',
                'default' => false,
                'scopes' => []
            ],

            Models\Role::USER => [
                'display' => 'Basic User',
                'email' => 'dummy@example.com',
                'default' => true,
                'scopes' => [
                    'home.index',
                    'account.show',
                    'account.update'
                ]
            ]
        ];
    }

    protected function settings()
    {
        return [
            'site_name' => [
                'type' => 'text',
                'value' => config('app.name')
            ],
            'site_logo' => [
                'type' => 'image',
                'value' => ''
            ],
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users() as $slug => $data) {
            $user = Models\User::create([
                'display' => $data['display'],
                'username' => $slug,
                'email' => $data['email'],
                'password' => 'secret',
                'active_at' => Carbon::now()
            ]);

            $role = Models\Role::create([
                'slug' => $slug,
                'is_default' => $data['default'],
                'scopes' => $data['scopes']
            ]);

            $user->roles()->attach($role->getKey());
        }

        foreach ($this->settings() as $slug => $setting) {
            Models\Setting::create([
                'slug' => $slug,
                'type' => $setting['type'],
                'value' => $setting['value'],
            ]);
        }
    }
}
