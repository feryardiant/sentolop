<?php

use Illuminate\Http\Request;

Route::group(['middleware' => 'guest'], function () {
    Route::post('login', 'AuthController@login')->name('auth.login');
    Route::post('register', 'AuthController@register')->name('auth.register');
    Route::post('forgot', 'AuthController@forgot')->name('auth.forgot');
    Route::post('reset', 'AuthController@reset')->name('auth.reset');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('home', 'HomeController@index')->name('home.index');

    Route::group(['prefix' => 'account'], function () {
        Route::get('', 'AccountController@show')->name('account.show');
        Route::match(['put', 'patch'], '', 'AccountController@update')->name('account.update');
    });

    Route::post('logout', 'AuthController@logout')->name('auth.logout');

    // Route::resources([]);
});
